package id.rastek.simpul.master;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.util.ResourceUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.TimeZone;

@SpringBootApplication
@EnableDiscoveryClient
public class MainApplication {

    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        this.getOrCreateConfiurationDirectory();
    }

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

    private void getOrCreateConfiurationDirectory() {
        try {
            String path = ResourceUtils.getFile("classpath:").getAbsolutePath();
            new File(path + "/configurations");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
