package id.rastek.simpul.master.controller;

import id.rastek.simpul.master.dto.LampuDto;
import id.rastek.simpul.master.model.Lampu;
import id.rastek.simpul.master.service.LampuService;
import id.rastek.simpul.master.util.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/node-lamp")
public class LampuController {

    @Autowired
    private LampuService shiftService;


    @GetMapping()
    public ResponseEntity<List<?>> list(LampuDto filter, @PageableDefault(sort = {"name"}, direction = Sort.Direction.ASC) final Pageable pageable) {
        try {
            Page<?> special = shiftService.getAll(filter, pageable);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<?>> list(LampuDto filter) {
        try {
            List<?> special = shiftService.getAll(filter);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        try {
            LampuDto special = shiftService.getById(id);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable(required = true) Long id, @RequestBody LampuDto input) {
        try {
            LampuDto result = shiftService.getById(id);
            if (result == null) {
                return ResponseEntity.notFound().build();
            }
            LampuDto special = shiftService.update(id, input);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }

    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody LampuDto input) {
        try {
            LampuDto special = shiftService.create(input);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable(required = true) Long id) {
        try {
            shiftService.delete(id);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", "Master Shift id " + id + " berhasil di hapus!"), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

}
