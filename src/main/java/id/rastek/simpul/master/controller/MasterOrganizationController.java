package id.rastek.simpul.master.controller;

import id.rastek.simpul.master.dto.MasterOrganizationDto;
import id.rastek.simpul.master.service.OrganizationService;
import id.rastek.simpul.master.util.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/organizations")
public class MasterOrganizationController {

    @Autowired
    private OrganizationService service;


    @GetMapping()
    public ResponseEntity<List<MasterOrganizationDto>> list(MasterOrganizationDto filter, @PageableDefault(sort = {"name"}, direction = Sort.Direction.ASC) final Pageable pageable) {
        try {
            Page<MasterOrganizationDto> special = service.getAll(filter, pageable);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<MasterOrganizationDto>> list(MasterOrganizationDto filter) {
        try {
            List<MasterOrganizationDto> special = service.getAll(filter);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        try {
            MasterOrganizationDto special = service.getById(id);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable(required = true) Long id, @RequestBody MasterOrganizationDto input) {
        try {
            MasterOrganizationDto result = service.getById(id);
            if (result == null) {
                return ResponseEntity.notFound().build();
            }
            MasterOrganizationDto special = service.update(id, input);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }

    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody MasterOrganizationDto input) {
        try {
            MasterOrganizationDto special = service.create(input);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable(required = true) Long id) {
        try {
            service.delete(id);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", "Organization id " + id + " berhasil di hapus!"), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

}
