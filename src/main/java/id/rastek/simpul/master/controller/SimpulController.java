package id.rastek.simpul.master.controller;

import id.rastek.simpul.master.dto.SimpulControllerDto;
import id.rastek.simpul.master.dto.SimpulControllerSearchDto;
import id.rastek.simpul.master.dto.SimpulUpdateLoraConfigDto;
import id.rastek.simpul.master.service.SimpulService;
import id.rastek.simpul.master.util.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/ref/simpul")
public class SimpulController {

    @Autowired
    private SimpulService simpulService;

    @GetMapping("/configuration/get")
    public ResponseEntity<SimpulControllerDto> list(SimpulControllerSearchDto searchDto) {
        try {
            SimpulControllerDto special = simpulService.getAll(searchDto);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @PostMapping("/configuration/post")
    public ResponseEntity<SimpulControllerDto> createOrReplaceConfig(@RequestBody SimpulUpdateLoraConfigDto configDto) {
        try {
            simpulService.createOrReplaceConfig(configDto);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", ""), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

}
