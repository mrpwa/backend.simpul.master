package id.rastek.simpul.master.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ConcentratorDto {

    private Long id;
    private String name;
    private String macAddress;
    private String kwhId;
    private String address;
    private String latitude;
    private String longitude;
    private String batteryBrand;
    private String batteryType;
    private String batteryBrandController;
    private String batteryControllerType;
    private Double batteryDayaPanel;
    private Integer batteryCapacity;
    private Integer batteryTotal;
    private String modemType;
    private String modemBrand;
    private String modemNoGsm;
    private String modemProvider;
    private String status;

    private Long organizationId;
    private String organizationName;

}