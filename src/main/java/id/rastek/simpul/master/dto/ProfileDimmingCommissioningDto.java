package id.rastek.simpul.master.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ProfileDimmingCommissioningDto {

    private Long id;
    private String name;
    private LocalTime startTime;
    private LocalTime endTime;
    private Integer dimming;
    private Integer lampStatus;

}