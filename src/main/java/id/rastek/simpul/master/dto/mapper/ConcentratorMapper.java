package id.rastek.simpul.master.dto.mapper;

import id.rastek.simpul.master.dto.ConcentratorDto;
import id.rastek.simpul.master.dto.mapper.config.MapperInjectConfig;
import id.rastek.simpul.master.model.Concentrator;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;

@Component
@Mapper(config = MapperInjectConfig.class)
public interface ConcentratorMapper {

    @Mappings({
            @Mapping(target = "organizationId", source = "organization.id"),
            @Mapping(target = "organizationName", source = "organization.name"),
            @Mapping(target = "status", source = "isOffline", qualifiedByName = "mappedConcentratorStatus")
    })
    public ConcentratorDto toDto(Concentrator entity);


    @Mappings({
    })
    public Concentrator toEntity(ConcentratorDto dto);

    @Named("mappedConcentratorStatus")
    default String mappedConcentratorStatus(Boolean isOffline) {
        if(isOffline == null) {
            return "";
        }
        return isOffline ? "OFFLINE" : "ONLINE";
    }
}
