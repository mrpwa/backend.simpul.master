package id.rastek.simpul.master.dto.mapper;

import id.rastek.simpul.master.dto.LampuDto;
import id.rastek.simpul.master.dto.mapper.config.MapperInjectConfig;
import id.rastek.simpul.master.model.Lampu;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

@Component
@Mapper(config = MapperInjectConfig.class)
public interface LampuMapper {

    @Mappings({
            @Mapping(target = "organizationId", source = "organization.id"),
            @Mapping(target = "organizationName", source = "organization.name"),
            @Mapping(target = "concentratorId", source = "concentrator.id"),
            @Mapping(target = "concentratorName", source = "concentrator.name"),
    })
    public LampuDto toDto(Lampu entity);


    @Mappings({
    })
    public Lampu toEntity(LampuDto dto);

}
