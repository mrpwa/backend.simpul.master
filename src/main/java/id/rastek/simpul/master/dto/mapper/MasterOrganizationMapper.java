package id.rastek.simpul.master.dto.mapper;

import com.google.common.base.Strings;
import id.rastek.simpul.master.dto.MasterOrganizationDto;
import id.rastek.simpul.master.dto.mapper.config.MapperInjectConfig;
import id.rastek.simpul.master.model.MasterOrganization;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

@Component
@Mapper(config = MapperInjectConfig.class)
public interface MasterOrganizationMapper {

    @Mappings({

    })
    public MasterOrganizationDto toDto(MasterOrganization entity);

    @Mappings({
    })
    public MasterOrganization toEntity(MasterOrganizationDto dto);


    @Named("byteToBase64String")
    default String byteToBase64String(byte[] logo) {
        if(logo == null) {
            return null;
        }
        return Base64Utils.encodeToString(logo);
    }

    @Named("base64StringToByte")
    default byte[] base64StringToByte(String logo) {
        if(Strings.isNullOrEmpty(logo)) {
            return null;
        }
        return Base64Utils.decodeFromString(logo);
    }


}
