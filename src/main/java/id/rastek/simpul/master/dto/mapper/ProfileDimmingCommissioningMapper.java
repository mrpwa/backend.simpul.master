package id.rastek.simpul.master.dto.mapper;

import id.rastek.simpul.master.dto.ProfileDimmingCommissioningDto;
import id.rastek.simpul.master.dto.mapper.config.MapperInjectConfig;
import id.rastek.simpul.master.model.ProfileDimmingCommissioning;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

@Component
@Mapper(config = MapperInjectConfig.class)
public interface ProfileDimmingCommissioningMapper {

    @Mappings({

    })
    public ProfileDimmingCommissioningDto toDto(ProfileDimmingCommissioning entity);


    @Mappings({
    })
    public ProfileDimmingCommissioning toEntity(ProfileDimmingCommissioningDto dto);

}
