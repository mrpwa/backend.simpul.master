package id.rastek.simpul.master.dto.mapper;

import id.rastek.simpul.master.dto.ProfileDimmingDto;
import id.rastek.simpul.master.dto.mapper.config.MapperInjectConfig;
import id.rastek.simpul.master.model.ProfileDimming;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

@Component
@Mapper(config = MapperInjectConfig.class)
public interface ProfileDimmingMapper {

    @Mappings({

    })
    public ProfileDimmingDto toDto(ProfileDimming entity);


    @Mappings({
    })
    public ProfileDimming toEntity(ProfileDimmingDto dto);

}
