package id.rastek.simpul.master.dto.mapper.config;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.MapperConfig;

@MapperConfig(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = InstantStringMapper.class)
public class MapperInjectConfig {
}
