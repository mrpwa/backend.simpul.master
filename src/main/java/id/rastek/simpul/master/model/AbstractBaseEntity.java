package id.rastek.simpul.master.model;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.repository.NoRepositoryBean;

/**
 *
 * @author nt
 * @param <U>
 */
@Getter
@Setter
@MappedSuperclass
@NoRepositoryBean
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractBaseEntity<U> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Version
    protected Long version;

    @CreatedDate
    @Column(name = "created_at", updatable = false)
    Instant createdAt;

    @LastModifiedDate
    @Column(name = "updated_at")
    Instant lastModifiedDate;

    @CreatedBy
    @Column(name = "created_by", length = 64, updatable = false)
    U createdBy;

    @LastModifiedBy
    @Column(name = "updated_by", length = 64)
    U lastModifiedBy;
}
