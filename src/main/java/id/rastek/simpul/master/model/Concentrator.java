package id.rastek.simpul.master.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="t_concentrator")
@EqualsAndHashCode(callSuper = false)
public class Concentrator extends AbstractBaseEntity<String> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ToString.Exclude
    @JsonIgnore
    @JoinColumn(name = "organization_id", referencedColumnName = "id")
    @ManyToOne
    private MasterOrganization organization;

    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @Column(name = "mac_address")
    private String macAddress;

    @Column(name = "kwh_id")
    private String kwhId;

    @Column(name = "address")
    private String address;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "is_offline")
    private Boolean isOffline;

    //#region Battery
    @Column(name = "battery_brand")
    private String batteryBrand;

    @Column(name = "battery_type")
    private String batteryType;

    @Column(name = "battery_brand_controller")
    private String batteryBrandController;

    @Column(name = "battery_controller_type")
    private String batteryControllerType;

    @Column(name = "battery_daya_panel")
    private Double batteryDayaPanel;   // watt/peak

    @Column(name = "battery_capacity")
    private Integer batteryCapacity;

    @Column(name = "battery_total")
    private Integer batteryTotal;
    //#endregion

    //#region Modem
    @Column(name = "modem_type")
    private String modemType;

    @Column(name = "modem_brand")
    private String modemBrand;

    @Column(name = "modem_no_gsm")
    private String modemNoGsm;

    @Column(name = "modem_provider")
    private String modemProvider;
    //#endregion


}
