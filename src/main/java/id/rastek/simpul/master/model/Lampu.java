package id.rastek.simpul.master.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="t_lamp")
@EqualsAndHashCode(callSuper = false)
public class Lampu extends AbstractBaseEntity<String> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ToString.Exclude
    @JsonIgnore
    @JoinColumn(name = "organization_id", referencedColumnName = "id")
    @ManyToOne
    private MasterOrganization organization;

    @ToString.Exclude
    @JsonIgnore
    @JoinColumn(name = "concentrator_id", referencedColumnName = "id")
    @ManyToOne
    private Concentrator concentrator;

    @Column(name = "serial_number")
    private String serialNumber;

    @Column(name = "serial_number_node")
    private String serialNumberNode;

    @Column(name = "brand")
    private String brand;

    @Column(name = "lumen")
    private Integer lumen;

    @Column(name = "daya")
    private Integer daya;

    @Column(name = "cct")
    private Integer cct;

    @Column(name = "tiang_name")
    private String tiangName;

    @Column(name = "tiang_height")
    private Double tiangHeight;

    @Column(name = "kwh_id")
    private String kwhId;

    @Column(name = "address")
    private String address;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "register_date")
    private LocalDate registerDate;

    //#region Battery
    @Column(name = "battery_brand")
    private String batteryBrand;

    @Column(name = "battery_type")
    private String batteryType;

    @Column(name = "battery_brand_controller")
    private String batteryBrandController;

    @Column(name = "battery_controller_type")
    private String batteryControllerType;

    @Column(name = "battery_daya_panel")
    private Double batteryDayaPanel;   // watt/peak

    @Column(name = "battery_capacity")
    private Integer batteryCapacity;

    @Column(name = "battery_total")
    private Integer batteryTotal;
    //#endregion


    @ToString.Exclude
    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "t_lamp_t_profile_dimming",
            joinColumns = @JoinColumn(name = "lamp_id"),
            inverseJoinColumns = @JoinColumn(name = "profile_dimming_id")
    )
    private List<ProfileDimming> profiles = new ArrayList<>();

}
