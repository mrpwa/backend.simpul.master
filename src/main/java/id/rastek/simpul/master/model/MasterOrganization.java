package id.rastek.simpul.master.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="m_organization")
@EqualsAndHashCode(callSuper = false)
public class MasterOrganization extends AbstractBaseEntity<String> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "name", nullable = false)
    private String name;

}
