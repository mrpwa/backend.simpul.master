package id.rastek.simpul.master.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="t_profile_dimming")
@EqualsAndHashCode(callSuper = false)
public class ProfileDimming extends AbstractBaseEntity<String> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @Column(name = "start_time")
    private LocalTime startTime;

    @Basic(optional = false)
    @Column(name = "dimming")
    private Integer dimming;

    @Basic(optional = false)
    @Column(name = "lamp_status")
    private Integer lampStatus;

}
