package id.rastek.simpul.master.repository;

import id.rastek.simpul.master.model.Concentrator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ConcentratorRepository extends JpaRepository<Concentrator, Long> {

    public Page<Concentrator> findAll(Pageable pageable);

    public Page<Concentrator> findAll(Specification<Concentrator> specification, Pageable pageable);

    public List<Concentrator> findAll();

    public List<Concentrator> findAll(Specification<Concentrator> specification);

}
