package id.rastek.simpul.master.repository;

import id.rastek.simpul.master.model.Lampu;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LampuRepository extends JpaRepository<Lampu, Long> {

    public Page<Lampu> findAll(Pageable pageable);

    public Page<Lampu> findAll(Specification<Lampu> specification, Pageable pageable);

    public List<Lampu> findAll();

    public List<Lampu> findAll(Specification<Lampu> specification);

}
