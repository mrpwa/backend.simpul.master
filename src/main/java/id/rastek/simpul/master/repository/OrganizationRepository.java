package id.rastek.simpul.master.repository;

import id.rastek.simpul.master.model.MasterOrganization;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrganizationRepository extends JpaRepository<MasterOrganization, Long> {

    public Page<MasterOrganization> findAll(Pageable pageable);

    public Page<MasterOrganization> findAll(Specification<MasterOrganization> specification, Pageable pageable);

    public List<MasterOrganization> findAll(Specification<MasterOrganization> specification);

    public MasterOrganization findFirstByNameEqualsIgnoreCase(String name);


}
