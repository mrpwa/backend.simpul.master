package id.rastek.simpul.master.repository;

import id.rastek.simpul.master.model.ProfileDimmingCommissioning;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileDimmingCommissioningRepository extends JpaRepository<ProfileDimmingCommissioning, Long> {

    public Page<ProfileDimmingCommissioning> findAll(Pageable pageable);

    public Page<ProfileDimmingCommissioning> findAll(Specification<ProfileDimmingCommissioning> specification, Pageable pageable);

    public List<ProfileDimmingCommissioning> findAll();

    public List<ProfileDimmingCommissioning> findAll(Specification<ProfileDimmingCommissioning> specification);

}
