package id.rastek.simpul.master.repository;

import id.rastek.simpul.master.model.ProfileDimming;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileDimmingRepository extends JpaRepository<ProfileDimming, Long> {

    public Page<ProfileDimming> findAll(Pageable pageable);

    public Page<ProfileDimming> findAll(Specification<ProfileDimming> specification, Pageable pageable);

    public List<ProfileDimming> findAll();

    public List<ProfileDimming> findAll(Specification<ProfileDimming> specification);

}
