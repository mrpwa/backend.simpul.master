package id.rastek.simpul.master.service;

import id.rastek.simpul.master.dto.ConcentratorDto;
import id.rastek.simpul.master.dto.mapper.ConcentratorMapper;
import id.rastek.simpul.master.model.Concentrator;
import id.rastek.simpul.master.model.MasterOrganization;
import id.rastek.simpul.master.repository.ConcentratorRepository;
import id.rastek.simpul.master.repository.OrganizationRepository;
import id.rastek.simpul.master.specification.ConcentratorSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional(readOnly = true)
public class ConcentratorService {

    @Autowired
    private ConcentratorRepository repo;
    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private ConcentratorMapper concentratorMapper;

    public List<ConcentratorDto> getAll() {
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .map(concentratorMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<ConcentratorDto> getAll(ConcentratorDto filter) {
        Specification<Concentrator> specification = ConcentratorSpecification.byKontrolerSearch(filter);
        return StreamSupport.stream(repo.findAll(specification).spliterator(), false)
                .map(concentratorMapper::toDto)
                .collect(Collectors.toList());
    }

    public Page<ConcentratorDto> getAll(ConcentratorDto filter, Pageable pageable) {
        Specification<Concentrator> specification = ConcentratorSpecification.byKontrolerSearch(filter);
        return repo.findAll(specification, pageable)
                .map(concentratorMapper::toDto);
    }

    public ConcentratorDto getById(Long id) {
        Concentrator entity = repo.findById(id).orElseThrow();
        return concentratorMapper.toDto(entity);
    }

    @Transactional(readOnly = false)
    public ConcentratorDto update(Long id, ConcentratorDto input) {
        Concentrator old = repo.findById(id).orElseThrow();
        Concentrator updater = concentratorMapper.toEntity(input);
        Concentrator updated = this.mapper(old, updater);
        updated = repo.save(updated);
        return concentratorMapper.toDto(updated);
    }

    @Transactional(readOnly = false)
    public ConcentratorDto create(ConcentratorDto input) {
        Concentrator entity = concentratorMapper.toEntity(input);
        entity = this.mapperRelation(entity, input);
        entity = repo.save(entity);
        return concentratorMapper.toDto(entity);
    }

    @Transactional(readOnly = false)
    public String delete(Long id) {
        Concentrator cat = repo.findById(id).orElseThrow();
        repo.deleteById(id);
        return "Kontroler id " + id + " nama " + cat.getName() + " telah dihapus.";
    }

    private Concentrator mapperRelation(Concentrator old, ConcentratorDto dto) {
        Concentrator ret = old;
        if (dto.getOrganizationId() != null) {
            MasterOrganization found = organizationRepository.findById(dto.getOrganizationId()).orElse(null);
            if (found != null) {
                ret.setOrganization(found);
            }
        }

        return ret;
    }

    private Concentrator mapper(Concentrator old, Concentrator updated) {
        Concentrator ret = old;
        if (updated.getName() != null && !updated.getName().isBlank()) {
            ret.setName(updated.getName());
        }
        if (updated.getMacAddress() != null && !updated.getMacAddress().isBlank()) {
            ret.setMacAddress(updated.getMacAddress());
        }
        if (updated.getKwhId() != null && !updated.getKwhId().isBlank()) {
            ret.setKwhId(updated.getKwhId());
        }
        if (updated.getAddress() != null && !updated.getAddress().isBlank()) {
            ret.setAddress(updated.getAddress());
        }
        if (updated.getLatitude() != null && !updated.getLatitude().isBlank()) {
            ret.setLatitude(updated.getLatitude());
        }
        if (updated.getLongitude() != null && !updated.getLongitude().isBlank()) {
            ret.setLongitude(updated.getLongitude());
        }
        if (updated.getIsOffline() != null) {
            ret.setIsOffline(updated.getIsOffline());
        }
        
        //#region Battery
        if (updated.getBatteryBrand() != null && !updated.getBatteryBrand().isBlank()) {
            ret.setBatteryBrand(updated.getBatteryBrand());
        }
        if (updated.getBatteryType() != null && !updated.getBatteryType().isBlank()) {
            ret.setBatteryType(updated.getBatteryType());
        }
        if (updated.getBatteryBrandController() != null && !updated.getBatteryBrandController().isBlank()) {
            ret.setBatteryBrandController(updated.getBatteryBrandController());
        }
        if (updated.getBatteryControllerType() != null && !updated.getBatteryControllerType().isBlank()) {
            ret.setBatteryControllerType(updated.getBatteryControllerType());
        }
        if (updated.getBatteryDayaPanel() != null) {
            ret.setBatteryDayaPanel(updated.getBatteryDayaPanel());
        }
        if (updated.getBatteryCapacity() != null) {
            ret.setBatteryCapacity(updated.getBatteryCapacity());
        }
        if (updated.getBatteryTotal() != null) {
            ret.setBatteryTotal(updated.getBatteryTotal());
        }
        //#endregion

        //#region Modem
        if (updated.getModemType() != null && !updated.getModemType().isBlank()) {
            ret.setModemType(updated.getModemType());
        }
        if (updated.getModemBrand() != null && !updated.getModemBrand().isBlank()) {
            ret.setModemBrand(updated.getModemBrand());
        }
        if (updated.getModemNoGsm() != null && !updated.getModemNoGsm().isBlank()) {
            ret.setModemNoGsm(updated.getModemNoGsm());
        }
        if (updated.getModemProvider() != null && !updated.getModemProvider().isBlank()) {
            ret.setModemProvider(updated.getModemProvider());
        }
        //#endregion

        return ret;
    }

}
