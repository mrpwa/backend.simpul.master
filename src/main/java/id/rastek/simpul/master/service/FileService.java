package id.rastek.simpul.master.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.rastek.simpul.master.dto.SimpulLoraDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

@Service
@Transactional(readOnly = true)
public class FileService {


    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    public SimpulLoraDto readJsonFromFile(String controllerId, String loraId) {
        try {
            File file = ResourceUtils.getFile("classpath:configurations/"+ controllerId + "/" +loraId);
            if(file.exists()) {
                SimpulLoraDto obj = this.getObjectMapper().readValue(file, SimpulLoraDto.class);
                obj.setLoraId(loraId);
                return obj;
            } else {
                return null;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getOrCreateControllerDirectory(String controllerId) throws FileNotFoundException {
        try {
            String path = ResourceUtils.getFile("classpath:configurations/" + controllerId).getAbsolutePath();
            return path;
        } catch (FileNotFoundException e) {
            String configPath = ResourceUtils.getFile("classpath:configurations/").getAbsolutePath();
            File controller = new File(configPath + "/" + controllerId);
            return controller.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String writeJsonToFile(String absolutePathController, String filename, Object value) {
        try {
            File json = new File(absolutePathController + "/" + filename.toUpperCase() + ".json");
            this.getObjectMapper().writeValue(json, value);
            return "success";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


}
