package id.rastek.simpul.master.service;

import id.rastek.simpul.master.dto.LampuDto;
import id.rastek.simpul.master.dto.mapper.LampuMapper;
import id.rastek.simpul.master.model.Concentrator;
import id.rastek.simpul.master.model.Lampu;
import id.rastek.simpul.master.model.MasterOrganization;
import id.rastek.simpul.master.model.ProfileDimming;
import id.rastek.simpul.master.repository.ConcentratorRepository;
import id.rastek.simpul.master.repository.LampuRepository;
import id.rastek.simpul.master.repository.OrganizationRepository;
import id.rastek.simpul.master.repository.ProfileDimmingRepository;
import id.rastek.simpul.master.specification.LampuSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional(readOnly = true)
public class LampuService {

    @Autowired
    private LampuRepository repo;
    @Autowired
    private ConcentratorRepository concentratorRepository;
    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private ProfileDimmingRepository profileDimmingRepository;
    @Autowired
    private LampuMapper lampuMapper;

    public List<LampuDto> getAll() {
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .map(lampuMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<LampuDto> getAll(LampuDto filter) {
        Specification<Lampu> specification = LampuSpecification.byLampuSearch(filter);
        return StreamSupport.stream(repo.findAll(specification).spliterator(), false)
                .map(lampuMapper::toDto)
                .collect(Collectors.toList());
    }

    public Page<LampuDto> getAll(LampuDto filter, Pageable pageable) {
        Specification<Lampu> specification = LampuSpecification.byLampuSearch(filter);
        return repo.findAll(specification, pageable)
                .map(lampuMapper::toDto);
    }

    public LampuDto getById(Long id) {
        Lampu entity = repo.findById(id).orElseThrow();
        return lampuMapper.toDto(entity);
    }

    @Transactional(readOnly = false)
    public LampuDto update(Long id, LampuDto input) {
        Lampu old = repo.findById(id).orElseThrow();
        Lampu updater = lampuMapper.toEntity(input);
        Lampu updated = this.mapper(old, updater);
        updated = this.mapperRelation(updated, input);
        updated = repo.save(updated);
        return lampuMapper.toDto(updated);
    }

    @Transactional(readOnly = false)
    public LampuDto create(LampuDto input) {
        Lampu entity = lampuMapper.toEntity(input);
        entity = this.mapperRelation(entity, input);
        entity = repo.save(entity);
        return lampuMapper.toDto(entity);
    }

    @Transactional(readOnly = false)
    public String delete(Long id) {
        Lampu cat = repo.findById(id).orElseThrow();
        repo.deleteById(id);
        return "Lampu id " + id + " nama " + cat.getTiangName() + " telah dihapus.";
    }

    private Lampu mapperRelation(Lampu old, LampuDto dto) {
        Lampu ret = old;
        if (dto.getOrganizationId() != null) {
            MasterOrganization found = organizationRepository.findById(dto.getOrganizationId()).orElse(null);
            if (found != null) {
                ret.setOrganization(found);
            }
        }

        if (dto.getConcentratorId() != null) {
            Concentrator found = concentratorRepository.findById(dto.getConcentratorId()).orElse(null);
            if (found != null) {
                ret.setConcentrator(found);
            }
        }

        if (dto.getProfiles() != null) {
            if (!dto.getProfiles().isEmpty()) old.getProfiles().clear();
            dto.getProfiles().forEach(q -> {
                ProfileDimming found = profileDimmingRepository.findById(q).orElse(null);
                if (found != null) {
                    old.getProfiles().add(found);
                }
            });
        }

        return ret;
    }

    private Lampu mapper(Lampu old, Lampu updated) {
        Lampu ret = old;
        if (updated.getSerialNumber() != null && !updated.getSerialNumber().isBlank()) {
            ret.setSerialNumber(updated.getSerialNumber());
        }
        if (updated.getSerialNumberNode() != null && !updated.getSerialNumberNode().isBlank()) {
            ret.setSerialNumberNode(updated.getSerialNumberNode());
        }
        if (updated.getBrand() != null && !updated.getBrand().isBlank()) {
            ret.setBrand(updated.getBrand());
        }
        if (updated.getTiangName() != null && !updated.getTiangName().isBlank()) {
            ret.setTiangName(updated.getTiangName());
        }
        if (updated.getKwhId() != null && !updated.getKwhId().isBlank()) {
            ret.setKwhId(updated.getKwhId());
        }
        if (updated.getAddress() != null && !updated.getAddress().isBlank()) {
            ret.setAddress(updated.getAddress());
        }
        if (updated.getLatitude() != null && !updated.getLatitude().isBlank()) {
            ret.setLatitude(updated.getLatitude());
        }
        if (updated.getLongitude() != null && !updated.getLongitude().isBlank()) {
            ret.setLongitude(updated.getLongitude());
        }
        if (updated.getLumen() != null) {
            ret.setLumen(updated.getLumen());
        }
        if (updated.getDaya() != null) {
            ret.setDaya(updated.getDaya());
        }
        if (updated.getCct() != null) {
            ret.setCct(updated.getCct());
        }
        if (updated.getTiangHeight() != null) {
            ret.setTiangHeight(updated.getTiangHeight());
        }
        if (updated.getRegisterDate() != null) {
            ret.setRegisterDate(updated.getRegisterDate());
        }
        
        //#region Battery
        if (updated.getBatteryBrand() != null && !updated.getBatteryBrand().isBlank()) {
            ret.setBatteryBrand(updated.getBatteryBrand());
        }
        if (updated.getBatteryType() != null && !updated.getBatteryType().isBlank()) {
            ret.setBatteryType(updated.getBatteryType());
        }
        if (updated.getBatteryBrandController() != null && !updated.getBatteryBrandController().isBlank()) {
            ret.setBatteryBrandController(updated.getBatteryBrandController());
        }
        if (updated.getBatteryControllerType() != null && !updated.getBatteryControllerType().isBlank()) {
            ret.setBatteryControllerType(updated.getBatteryControllerType());
        }
        if (updated.getBatteryDayaPanel() != null) {
            ret.setBatteryDayaPanel(updated.getBatteryDayaPanel());
        }
        if (updated.getBatteryCapacity() != null) {
            ret.setBatteryCapacity(updated.getBatteryCapacity());
        }
        if (updated.getBatteryTotal() != null) {
            ret.setBatteryTotal(updated.getBatteryTotal());
        }
        //#endregion

        return ret;
    }

}
