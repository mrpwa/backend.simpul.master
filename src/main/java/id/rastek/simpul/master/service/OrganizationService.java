package id.rastek.simpul.master.service;

import id.rastek.simpul.master.dto.MasterOrganizationDto;
import id.rastek.simpul.master.dto.mapper.MasterOrganizationMapper;
import id.rastek.simpul.master.model.MasterOrganization;
import id.rastek.simpul.master.repository.OrganizationRepository;
import id.rastek.simpul.master.specification.MasterOrganizationSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional(readOnly = true)
public class OrganizationService {

    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private MasterOrganizationMapper organizationMapper;

    public List<MasterOrganizationDto> getAll() {
        return StreamSupport.stream(organizationRepository.findAll().spliterator(), false)
                .map(organizationMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<MasterOrganizationDto> getAll(MasterOrganizationDto filter) {
        Specification<MasterOrganization> specification = MasterOrganizationSpecification.byMasterOrganizationSearch(filter);
        return StreamSupport.stream(organizationRepository.findAll(specification).spliterator(), false)
                .map(organizationMapper::toDto)
                .collect(Collectors.toList());
    }

    public Page<MasterOrganizationDto> getAll(MasterOrganizationDto filter, Pageable pageable) {
        Specification<MasterOrganization> specification = MasterOrganizationSpecification.byMasterOrganizationSearch(filter);
        return organizationRepository.findAll(specification, pageable)
                .map(organizationMapper::toDto);
    }

    public MasterOrganizationDto getById(Long id) {
        MasterOrganization MasterOrganization = organizationRepository.findById(id).orElseThrow();
        return organizationMapper.toDto(MasterOrganization);
    }

    @Transactional(readOnly = false)
    public MasterOrganizationDto update(Long id, MasterOrganizationDto input) {
        MasterOrganization old = organizationRepository.findById(id).orElseThrow();
        MasterOrganization updater = organizationMapper.toEntity(input);
        MasterOrganization updated = this.mapper(old, updater);
        updated = organizationRepository.save(updated);
        return organizationMapper.toDto(updated);
    }

    @Transactional(readOnly = false)
    public MasterOrganizationDto create(MasterOrganizationDto input) {
        MasterOrganization MasterOrganization = organizationMapper.toEntity(input);
        MasterOrganization = organizationRepository.save(MasterOrganization);
        return organizationMapper.toDto(MasterOrganization);
    }

    @Transactional(readOnly = false)
    public String delete(Long id) {
        MasterOrganization cat = organizationRepository.findById(id).orElseThrow();
        organizationRepository.deleteById(id);
        return "Master Department id " + id + " nama " + cat.getName() + " telah dihapus.";
    }

    private MasterOrganization mapper(MasterOrganization old, MasterOrganization updated) {
        MasterOrganization ret = old;
        if (updated.getName() != null && !updated.getName().isBlank()) {
            ret.setName(updated.getName());
        }

        return ret;
    }

}
