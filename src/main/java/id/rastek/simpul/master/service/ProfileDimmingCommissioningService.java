package id.rastek.simpul.master.service;

import id.rastek.simpul.master.dto.ProfileDimmingCommissioningDto;
import id.rastek.simpul.master.dto.mapper.ProfileDimmingCommissioningMapper;
import id.rastek.simpul.master.model.ProfileDimmingCommissioning;
import id.rastek.simpul.master.repository.ProfileDimmingCommissioningRepository;
import id.rastek.simpul.master.specification.ProfileDimmingCommissioningSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional(readOnly = true)
public class ProfileDimmingCommissioningService {

    @Autowired
    private ProfileDimmingCommissioningRepository repo;
    @Autowired
    private ProfileDimmingCommissioningMapper profileDimmingMapper;

    public List<ProfileDimmingCommissioningDto> getAll() {
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .map(profileDimmingMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<ProfileDimmingCommissioningDto> getAll(ProfileDimmingCommissioningDto filter) {
        Specification<ProfileDimmingCommissioning> specification = ProfileDimmingCommissioningSpecification.byProfileDimmingCommissioningSearch(filter);
        return StreamSupport.stream(repo.findAll(specification).spliterator(), false)
                .map(profileDimmingMapper::toDto)
                .collect(Collectors.toList());
    }

    public Page<ProfileDimmingCommissioningDto> getAll(ProfileDimmingCommissioningDto filter, Pageable pageable) {
        Specification<ProfileDimmingCommissioning> specification = ProfileDimmingCommissioningSpecification.byProfileDimmingCommissioningSearch(filter);
        return repo.findAll(specification, pageable)
                .map(profileDimmingMapper::toDto);
    }

    public ProfileDimmingCommissioningDto getById(Long id) {
        ProfileDimmingCommissioning entity = repo.findById(id).orElseThrow();
        return profileDimmingMapper.toDto(entity);
    }

    @Transactional(readOnly = false)
    public ProfileDimmingCommissioningDto update(Long id, ProfileDimmingCommissioningDto input) {
        ProfileDimmingCommissioning old = repo.findById(id).orElseThrow();
        ProfileDimmingCommissioning updater = profileDimmingMapper.toEntity(input);
        ProfileDimmingCommissioning updated = this.mapper(old, updater);
        updated = repo.save(updated);
        return profileDimmingMapper.toDto(updated);
    }

    @Transactional(readOnly = false)
    public ProfileDimmingCommissioningDto create(ProfileDimmingCommissioningDto input) {
        ProfileDimmingCommissioning entity = profileDimmingMapper.toEntity(input);
        entity = repo.save(entity);
        return profileDimmingMapper.toDto(entity);
    }

    @Transactional(readOnly = false)
    public String delete(Long id) {
        ProfileDimmingCommissioning cat = repo.findById(id).orElseThrow();
        repo.deleteById(id);
        return "Profile Dimming id " + id + " nama " + cat.getName() + " telah dihapus.";
    }

    private ProfileDimmingCommissioning mapper(ProfileDimmingCommissioning old, ProfileDimmingCommissioning updated) {
        ProfileDimmingCommissioning ret = old;
        if (updated.getName() != null && !updated.getName().isBlank()) {
            ret.setName(updated.getName());
        }
        if (updated.getStartTime() != null) {
            ret.setStartTime(updated.getStartTime());
        }
        if (updated.getEndTime() != null) {
            ret.setEndTime(updated.getEndTime());
        }
        if (updated.getLampStatus() != null) {
            ret.setLampStatus(updated.getLampStatus());
        }
        if (updated.getDimming() != null) {
            ret.setDimming(updated.getDimming());
        }

        return ret;
    }

}
