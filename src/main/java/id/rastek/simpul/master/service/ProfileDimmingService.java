package id.rastek.simpul.master.service;

import id.rastek.simpul.master.dto.ProfileDimmingDto;
import id.rastek.simpul.master.dto.mapper.ProfileDimmingMapper;
import id.rastek.simpul.master.model.ProfileDimming;
import id.rastek.simpul.master.repository.ProfileDimmingRepository;
import id.rastek.simpul.master.specification.ProfileDimmingSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional(readOnly = true)
public class ProfileDimmingService {

    @Autowired
    private ProfileDimmingRepository repo;
    @Autowired
    private ProfileDimmingMapper profileDimmingMapper;

    public List<ProfileDimmingDto> getAll() {
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .map(profileDimmingMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<ProfileDimmingDto> getAll(ProfileDimmingDto filter) {
        Specification<ProfileDimming> specification = ProfileDimmingSpecification.byProfileDimmingSearch(filter);
        return StreamSupport.stream(repo.findAll(specification).spliterator(), false)
                .map(profileDimmingMapper::toDto)
                .collect(Collectors.toList());
    }

    public Page<ProfileDimmingDto> getAll(ProfileDimmingDto filter, Pageable pageable) {
        Specification<ProfileDimming> specification = ProfileDimmingSpecification.byProfileDimmingSearch(filter);
        return repo.findAll(specification, pageable)
                .map(profileDimmingMapper::toDto);
    }

    public ProfileDimmingDto getById(Long id) {
        ProfileDimming entity = repo.findById(id).orElseThrow();
        return profileDimmingMapper.toDto(entity);
    }

    @Transactional(readOnly = false)
    public ProfileDimmingDto update(Long id, ProfileDimmingDto input) {
        ProfileDimming old = repo.findById(id).orElseThrow();
        ProfileDimming updater = profileDimmingMapper.toEntity(input);
        ProfileDimming updated = this.mapper(old, updater);
        updated = repo.save(updated);
        return profileDimmingMapper.toDto(updated);
    }

    @Transactional(readOnly = false)
    public ProfileDimmingDto create(ProfileDimmingDto input) {
        ProfileDimming entity = profileDimmingMapper.toEntity(input);
        entity = repo.save(entity);
        return profileDimmingMapper.toDto(entity);
    }

    @Transactional(readOnly = false)
    public String delete(Long id) {
        ProfileDimming cat = repo.findById(id).orElseThrow();
        repo.deleteById(id);
        return "Profile Dimming id " + id + " nama " + cat.getName() + " telah dihapus.";
    }

    private ProfileDimming mapper(ProfileDimming old, ProfileDimming updated) {
        ProfileDimming ret = old;
        if (updated.getName() != null && !updated.getName().isBlank()) {
            ret.setName(updated.getName());
        }
        if (updated.getStartTime() != null) {
            ret.setStartTime(updated.getStartTime());
        }
        if (updated.getLampStatus() != null) {
            ret.setLampStatus(updated.getLampStatus());
        }
        if (updated.getDimming() != null) {
            ret.setDimming(updated.getDimming());
        }

        return ret;
    }

}
