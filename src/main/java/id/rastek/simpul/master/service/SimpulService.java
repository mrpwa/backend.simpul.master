package id.rastek.simpul.master.service;

import id.rastek.simpul.master.dto.SimpulControllerDto;
import id.rastek.simpul.master.dto.SimpulControllerSearchDto;
import id.rastek.simpul.master.dto.SimpulLoraDto;
import id.rastek.simpul.master.dto.SimpulUpdateLoraConfigDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileNotFoundException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class SimpulService {

    @Autowired
    FileService fileService;

    public SimpulControllerDto getAll(SimpulControllerSearchDto dto) {
        String controllerId = dto.getControllerId();
        List<String> loraIds = dto.getLoraIds();

        SimpulControllerDto controller = new SimpulControllerDto();
        controller.setControllerId(controllerId.toUpperCase());

        for (String loraId : loraIds) {
            loraId = loraId.toUpperCase() + ".json";
            SimpulLoraDto loraDto = fileService.readJsonFromFile(controllerId.toUpperCase(), loraId);
            controller.getLoras().add(loraDto);
        }
        return controller;
    }

    public void createOrReplaceConfig(SimpulUpdateLoraConfigDto dto) throws FileNotFoundException {
        String controllerId = dto.getControllerId();
        List<String> loraIds = dto.getLoraIds();

        String controllerPath = fileService.getOrCreateControllerDirectory(controllerId.toUpperCase());
        for (String loraId : loraIds) {
            fileService.writeJsonToFile(controllerPath, loraId, dto.getLoraConfig());
        }
    }

}
