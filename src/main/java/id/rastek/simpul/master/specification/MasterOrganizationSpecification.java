package id.rastek.simpul.master.specification;

import id.rastek.simpul.master.dto.MasterOrganizationDto;
import id.rastek.simpul.master.model.MasterOrganization;
import org.springframework.data.jpa.domain.Specification;

public class MasterOrganizationSpecification {
    
    private static Specification<MasterOrganization> siteAttributeContains(String attribute, String value) {
        if (value != null && !value.isBlank()) {
            final String finalValue = "%" + value.toLowerCase() + "%";
            return (root, query, cb) -> {
                return cb.like(
                        cb.lower(root.get(attribute)),
                        finalValue
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<MasterOrganization> siteAttributeNotEqual(String attribute, String value) {
        if (value != null && !value.isBlank()) {
            final String finalText = value.toLowerCase();
            return (root, query, cb) -> {
                return cb.notEqual(
                        cb.lower(root.get(attribute)),
                        finalText
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<MasterOrganization> byName(String name) {
        return siteAttributeContains("name", name);
    }

    public static Specification<MasterOrganization> byMasterOrganizationSearch(MasterOrganizationDto params) {
        if (params == null) {
            return null;
        }
        return byName(params.getName())
                ;
    }

}
