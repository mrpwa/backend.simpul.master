package id.rastek.simpul.master.specification;

import com.google.common.base.Strings;
import id.rastek.simpul.master.dto.ProfileDimmingCommissioningDto;
import id.rastek.simpul.master.model.ProfileDimmingCommissioning;
import id.rastek.simpul.master.util.enums.ControllerType;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public class ProfileDimmingCommissioningSpecification {
    
    private static Specification<ProfileDimmingCommissioning> siteAttributeContains(String attribute, String value) {
        if (value != null && !value.isBlank()) {
            final String finalValue = "%" + value.toLowerCase() + "%";
            return (root, query, cb) -> {
                return cb.like(
                        cb.lower(root.get(attribute)),
                        finalValue
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<ProfileDimmingCommissioning> siteAttributeEqual(String attribute, Long value) {
        if (value != null) {
            final Long finalText = value;
            return (root, query, cb) -> {
                return cb.equal(
                        root.get(attribute),
                        finalText
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<ProfileDimmingCommissioning> siteAttributeIn(String attribute, List<Long> values) {
        if (values != null) {
            if (!values.isEmpty()) {
                return (root, query, cb) -> {
                    CriteriaBuilder.In<Long> inClause = cb.in(root.get(attribute));
                    for (Long value : values) {
                        inClause.value(value);
                    }
                    return inClause;
                };
            }
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<ProfileDimmingCommissioning> siteAttributeNotEqual(String attribute, String value) {
        if (value != null && !value.isBlank()) {
            final String finalText = value.toLowerCase();
            return (root, query, cb) -> {
                return cb.notEqual(
                        cb.lower(root.get(attribute)),
                        finalText
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<ProfileDimmingCommissioning> siteAttributeEnumStringEquals(String attribute, String value) {
        if (value != null && !value.isBlank()) {
            final ControllerType status = ControllerType.valueOf(value.toUpperCase());
            return (root, query, cb) -> {
                return cb.equal(
                        root.get(attribute),
                        status
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<ProfileDimmingCommissioning> relationAttributeContains(String attribute, String param) {
        if (!Strings.isNullOrEmpty(param)) {
            final String finalValue = "%" + param.toLowerCase() + "%";
            String[] memberParts = attribute.split("\\.");
            return (root, query, cb) -> {
                return cb.like(
                        cb.lower(root.get(memberParts[0]).get(memberParts[1])),
                        finalValue
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<ProfileDimmingCommissioning> relationAttributeEquals(String attribute, Long param) {
        if (param != null) {
            final Long finalValue = param;
            String[] memberParts = attribute.split("\\.");
            return (root, query, cb) -> {
                return cb.equal(
                        root.get(memberParts[0]).get(memberParts[1]),
                        finalValue
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<ProfileDimmingCommissioning> byName(String name) {
        return siteAttributeContains("name", name);
    }


    public static Specification<ProfileDimmingCommissioning> byProfileDimmingCommissioningSearch(ProfileDimmingCommissioningDto params) {
        if (params == null) {
            return null;
        }
        return byName(params.getName())
                ;
    }

}
