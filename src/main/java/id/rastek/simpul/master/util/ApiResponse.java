package id.rastek.simpul.master.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse <T extends Object> implements Serializable {

    private String code;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String error;
    private String message;
    private T data;

}
