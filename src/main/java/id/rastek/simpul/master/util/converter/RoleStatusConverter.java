package id.rastek.simpul.master.util.converter;

import id.rastek.simpul.master.util.enums.ControllerType;

import javax.persistence.AttributeConverter;

public class RoleStatusConverter implements AttributeConverter<ControllerType, String> {

    @Override
    public String convertToDatabaseColumn(ControllerType taskStatus) {
        return taskStatus.toString();
    }

    @Override
    public ControllerType convertToEntityAttribute(String taskStatus) {
        return ControllerType.valueOf(taskStatus);
    }

}
