package id.rastek.simpul.master.util.enums;

public enum ControllerType {
    MPPT(1), PWM(2);
    Integer value;

    private ControllerType(final int newValue) {
        this.value = newValue;
    }

    private ControllerType() {
    }
}